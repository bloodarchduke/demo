export default ()=>{
  return [
    {
      id:1,
      name: "Cream tea",
      description:"This is a cup of cream tea",
      imageUrl:"https://culinaryginger.com/wp-content/uploads/2014/11/afternoon-tea-2.jpg",
      created_date:"2018-20-3"
    },
    {
      id:2,
      name: "Sashimi",
      description:"This is sashimi",
      imageUrl:"http://www.makesushi.com/wp-content/uploads/2015/05/sashimi-recipe-topimg.jpg",
      created_date:"2018-20-3"
    },
    {
      id:3,
      name: "takoyaki",
      description:"This is takoyaki",
      imageUrl:"https://www.justonecookbook.com/wp-content/uploads/2013/10/Takoyaki-NEW.jpg",
      created_date:"2018-20-3"
    },
    {
      id:4,
      name: "tempura",
      description:"This is tempura",
      imageUrl:"http://seonkyounglongest.com/wp-content/uploads/2017/02/IMG_0448-1000x562.jpg?x61413",
      created_date:"2018-20-3"
    },
    {
      id:5,
      name: "ramen",
      description:"This is ramen",
      imageUrl:"https://img.taste.com.au/HIPthOUd/taste/2017/08/beeframen-129052-1.jpg",
      created_date:"2018-20-3"
    }
  ]
}
