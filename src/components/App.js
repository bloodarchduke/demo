import React, { Component } from 'react';
import './App.css';
import FoodContainer from "../containers/food-list"
import FoodDetailContainer from "../containers/food-detail";

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Welcome to Hung Restaurant</h2>
        </div>
        <p className="App-intro">
          <p>This ís Menu</p>
        </p>
          <h2 className="List_of_foods">List of foods: </h2>
            <h4 className="tips">Please click on name of food to see description</h4>
            <FoodContainer/>
          <hr/>
          <h2 className="food_details">Food details: </h2>
            <FoodDetailContainer/>
      </div>
    );
  }
}

export default App;
