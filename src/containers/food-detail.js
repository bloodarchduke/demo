import React,{Component} from "react";
import {connect} from "react-redux";
import './food-detail.css';

class FoodDetail extends Component {
  render(){
    if (!this.props.activeFood){
      return (<h2>Select a food</h2>);
    }
    return(
      <div>
        <img src={this.props.activeFood.imageUrl ?  this.props.activeFood.imageUrl : ""}
        height={this.props.activeFood.imageUrl ? 300 : 0}
        width={this.props.activeFood.imageUrl ? 300 : 0}/>
        <p className="food-name">Name: {this.props.activeFood.name}</p>
        <p className="description">Description: {this.props.activeFood.description}</p>
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
    activeFood: state.activeFood
  };
}

let FoodDetailContainer = connect(mapStateToProps)(FoodDetail);

export default FoodDetailContainer;
